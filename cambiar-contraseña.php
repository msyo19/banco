<!DOCTYPE html>
<html lang="en">
<?php
session_start();
$cuenta = $_SESSION['inicio']['cuenta'];
require_once "conexion/conexion.php";
$_query = "SELECT clave FROM cuenta where cuenta = $cuenta  ";
$result = $mysqli->query($_query);
   if (!$result){
    die($mysqli->error);
   }
   $rows = $result->num_rows;
   for ($j = 0 ; $j < $rows ; ++$j){
   $result->data_seek($j);
   $row = $result->fetch_array(MYSQLI_ASSOC);
   $clave =  $row['clave'];
}
$result->close();

 ?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Inicio</title>
  <script src="conexion\jquery-3.3.1.min.js" charset="utf-8"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <style>
    .nav{
background-color: #0B0B61;
padding: 10px;
}
#Bancomilenio{
    color: white;
    font-size: 30px;
}
#servicios{
    font-family: Arial, Helvetica, sans-serif;
}
.btn{
    background-color: yellowgreen;
    padding-right: 20px;
    color: white;
    margin-left: 110px;
    font-size: 20px;
}
.btn:hover{
    background: #0B0B61;
    color: white;
}
body{
    background: #abbaab;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #ffffff, #abbaab);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #ffffff, #abbaab); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
#pago{
    float: left;
    margin-right: 30px
}
.logout{

background-color: #0B0B61;
color: white;
font-size: 20px;

}
.titulo{
    font-size: 40px;
    color: #0B0B61;

}

</style>
</head>


<body>
  <nav class="nav">
    <a id="Bancomilenio" class="navbar-brand" href="main.html">
      <img src="imgs/logo.svg" width="50" height="50" class="d-inline-block align-top" alt="logo">
      Banca Milenio
    </a>
    <a href="logout.php" class="logout">Cerrar sesión</a>
  </nav>
  <br>
  <h1 class="titulo text-center">Cambiar contraseña</h1>
  <br>
  <form id="contra">
    <div class="form-row justify-content-center">
      <div class="col-md-4 mb-3">
        <label for="validationDefault01">Contraseña actual</label>
        <input type="text" class="form-control" id="c3" placeholder="Contraseña" value="" required>
      </div>
    </div>
    <div class="form-row justify-content-center">
      <div class="col-md-4 mb-3">
        <label for="validationDefault01">Nueva contraseña</label>
        <input type="text" class="form-control" id="c1" name="c1"placeholder="Contraseña nueva" value="" required>
      </div>
    </div>
    <div class="form-row justify-content-center">
      <div class="col-md-4 mb-3">
        <label for="validationDefault01">Nueva contraseña</label>
        <input type="text" class="form-control" id="c2" placeholder="Contraseña nueva" value="" required>
      </div>
    </div>
      <br>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-4">
          <input type="submit" class="btn btn-lg" value="Cambiar">
        </div>
      </div>
    </div>
  </form>

  <script type="text/javascript">

    var clave = "<?php echo $clave ?>";
    var password, password2;

    password = document.getElementById('c1');
    password2 = document.getElementById('c2');
    password3 = document.getElementById('c3');
    password.onchange = password2.onkeyup = passwordMatch;

    function passwordMatch() {
      if (password.value !== password2.value)
        password2.setCustomValidity('Las contraseñas no coinciden.');
      else
        password2.setCustomValidity('');
    }


    jQuery(document).on('submit', '#contra', function(event) {
      event.preventDefault();
      if (password3.value===clave) {
        jQuery.ajax({
            url: 'conexion/contra.php',
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function() {

            }
          })
          .done(function(respuesta) {
            console.log(respuesta);
            location.href="main.html";
          })
          .fail(function(resp) {
            console.log(resp.responseText);
          })
          .always(function() {

          });
        }
    });
  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
