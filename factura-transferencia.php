<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title> Factura de pago </title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <style>
    .nav {
      background-color: #0B0B61;
      padding: 10px;
    }

    #Bancomilenio {
      color: white;
      font-size: 30px;
    }

    #servicios {
      font-family: Arial, Helvetica, sans-serif;
    }

    .btn {
      background-color: yellowgreen;
      padding-right: 20px;
      color: white;
      margin-left: 110px;
    }

    .btn:hover {
      background: #0B0B61;
      color: white;
    }

    body {
      background: #abbaab;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #ffffff, #abbaab);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #ffffff, #abbaab);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    #pago {
      float: left;
      margin-right: 30px
    }

    .logout {

      background-color: #0B0B61;
      color: white;
      font-size: 20px;

    }
    #factura-pago{
      background-color: #abbaab;
      color: white;
      padding: 20px;
    }
  </style>
</head>
<body>
  <nav class="nav">
    <a id="Bancomilenio" class="navbar-brand" href="main.html">
      <img src="imgs/logo.svg" width="50" height="50" class="d-inline-block align-top" alt="logo">
      Banca Milenio
    </a>
    <a href="logout.php" class="logout">Cerrar sesión</a>
  </nav>
  <br>
  <div id="servicios" class="container">
    <div class="row justify-content-center">
      <h1>
     Factura de pago
      </h1>
    </div>
  </div>
  </div>
  <br>
  <div class="container" id="factura-pago">
    <div class="row justify-content-center">
      <div class="">
      <h3>Factura de pago aquí</h3>
      <!-- Aqui se va a desplegar
      -fecha de transacción
      -el cargo
      -impuesto(IVA)
      -TOTAL A PAGAR:
      -Saldo actual:
      -->
      <b>Cantidad de transferencia: $</b>
      <?php
      session_start();
      echo $_SESSION['pagos']['cantidad']; ?>
      <br>
      <b>Fecha de transferencia: </b>
      <?php
      echo date("D");?> <?php
      echo date("d");?> <?php
      echo date("F");?> <?php
      echo date("Y");?> <?php
      $time = time();
      echo date("(H:i:s)", $time)
       ?>
       <br>
       <b>Saldo actual: </b>
       <?php
       $cuenta = $_SESSION['inicio']['cuenta'];
       require_once "conexion/conexion.php";
       $_query = "SELECT saldo FROM cuenta where cuenta = $cuenta  ";
      $result = $mysqli->query($_query);
          if (!$result){
           die($mysqli->error);
          }
          $rows = $result->num_rows;
          for ($j = 0 ; $j < $rows ; ++$j){
          $result->data_seek($j);
          $row = $result->fetch_array(MYSQLI_ASSOC);
          echo "$".$row['saldo'];
       }
      $result->close();
        ?>
      </div>
    </div>
  </div>
  <br>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-4">
  <a href="main.html" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Regresar</a>
  </div>
</div>
</div>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
