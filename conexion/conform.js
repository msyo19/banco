jQuery(document).on('submit', '#login', function(event) {
  event.preventDefault();
  jQuery.ajax({
      url: 'conexion/cus.php',
      type: 'POST',
      dataType: 'json',
      data: $(this).serialize(),
      beforeSend: function() {
        $('#ingresar').val('Validando...');
      }
    })
    .done(function(respuesta) {
      $('.falla').text('Usuario o contraseña incorrectos');
      if (!respuesta.error) {
        console.log(respuesta);
        location.href="main.html";
      } else {
        $('.falla').slideDown('slow');
        setTimeout(function() {
          $('.falla').slideUp()
        }, 3000);
        console.error("Usuario o contraseña incorrectos");
      }
    })
    .fail(function(resp) {
      console.log(resp.responseText);
      $('.falla').text('A ocurrido un error: '+ resp.responseText);
      $('.falla').slideDown('slow');
      setTimeout(function() {
        $('.falla').slideUp()
      }, 3000);
    })
    .always(function() {
      setTimeout(function() {
        $('#ingresar').val('Ingresar')
      }, 3000);
    });
});
