<!DOCTYPE html>
<?php
session_start();
$cuenta = $_SESSION['inicio']['cuenta'];
 ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style>
.nav{
background-color: #0B0B61;
padding: 10px;
}
#Bancomilenio{
    color: white;
    font-size: 30px;
}
#servicios{
    font-family: Arial, Helvetica, sans-serif;
}
.btn{
    background-color: yellowgreen;
    padding-right: 20px;
    color: white;
    margin-left: 110px;
    font-size: 20px;
}
.btn:hover{
    background: #0B0B61;
    color: white;
}
body{
    background: #abbaab;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #ffffff, #abbaab);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #ffffff, #abbaab); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
#pago{
    float: left;
    margin-right: 30px
}
.logout{

background-color: #0B0B61;
color: white;
font-size: 20px;

}
.titulo{
    font-size: 40px;
    color: #0B0B61;

}
#estadocuenta{
    background-color: white;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}


</style>
</head>


<body>
    <nav class="nav">
        <a  id="Bancomilenio" class="navbar-brand" href="main.html">
            <img src="imgs/logo.svg" width="50" height="50" class="d-inline-block align-top" alt="logo">
                Banca Milenio
        </a>
      <a href="logout.php" class="logout">Cerrar sesión</a>
    </nav>
 <br>
 <h1 class="text-center">Estado de cuenta</h1>
 <br>
 <div class="container" id="estadocuenta">
     <br>
     <div class="row">
         <div class="col-sm-12 col col-md-4">
         <h4>Nombre <span class="badge badge-secondary"><?php echo $_SESSION['inicio']['nombre']; ?></span></h4>
           </div>

     <div class="col-ms-12 col-md-4">
        <h4>Saldo Actual <span class="badge badge-secondary">
          <?php
          require_once "conexion/conexion.php";
          $_query = "SELECT saldo FROM cuenta where cuenta = $cuenta  ";
         $result = $mysqli->query($_query);
             if (!$result){
              die($mysqli->error);
             }
             $rows = $result->num_rows;
             for ($j = 0 ; $j < $rows ; ++$j){
             $result->data_seek($j);
             $row = $result->fetch_array(MYSQLI_ASSOC);
             echo "$".$row['saldo'];
          }
         $result->close();
           ?>
        </span></h4>
     </div>
     <div class="col-ms-12 col-md-4">
        <h4>No. de sucursal <span class="badge badge-secondary"><?php echo $_SESSION['inicio']['sucursal']; ?></span></h4>
     </div>
 </div>
 <br>
 <div class="row">
     <div class="col-sm-12 col-md-4">
        <h4>No. de cuenta <span class="badge badge-secondary"><?php echo $_SESSION['inicio']['cuenta']; ?></span></h4>
     </div>
     <div class="col-sm-12 col-md-4">
        <h4>Clave <span class="badge badge-secondary">
          <?php
          require_once "conexion/conexion.php";
          $_query = "SELECT clave FROM cuenta where cuenta = $cuenta  ";
         $result = $mysqli->query($_query);
             if (!$result){
              die($mysqli->error);
             }
             $rows = $result->num_rows;
             for ($j = 0 ; $j < $rows ; ++$j){
             $result->data_seek($j);
             $row = $result->fetch_array(MYSQLI_ASSOC);
             echo $row['clave'];
          }
         $result->close();
           ?>
        </span></h4>
     </div>
     <div class="col-sm-12 col-md-4">
        <h4>No. de cliente <span class="badge badge-secondary"><?php echo $_SESSION['inicio']['cliente']; ?></span></h4>
     </div>
 </div><br>
 </div>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-4">
<a href="main.html" class="btn  btn-lg ">Regresar</a>
</div>
</div>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
